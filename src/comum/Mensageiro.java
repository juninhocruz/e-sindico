package comum;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Mensageiro extends Remote {
	public String hello(String nome) throws RemoteException;
	
	public List<Transacao> saque(Usuario usuario, double valor) throws RemoteException;
	
	public List<Transacao> deposito(Usuario usuario, double valor) throws RemoteException;
	
	public List<Transacao> addUpdateListener(UpdateListener listener) throws RemoteException;
}
