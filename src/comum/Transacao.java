package comum;

import java.io.Serializable;

public class Transacao implements Serializable {
	private static final long serialVersionUID = 1L;
	private Usuario sindico;
	private double valor;
	private Tipo tipo;
	private double saldo;
	
	public enum Tipo {
		DEPOSITO, SAQUE
	}

	public Transacao(Usuario sindico, double valor, Tipo tipo, double saldo) {
		super();
		this.sindico = sindico;
		this.valor = valor;
		this.tipo = tipo;
		this.saldo = saldo;
	}
	
	@Override
	public String toString() {
		String action = tipo == Tipo.DEPOSITO?"depositou":"sacou";
		return "'" + sindico.getNome() + "' " + action + " R$" + valor + ".\nSaldo Atual: R$" + saldo + "\n\n";
		
	}

	public Usuario getSindico() {
		return sindico;
	}

	public void setSindico(Usuario sindico) {
		this.sindico = sindico;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
}
