package comum;

import java.io.Serializable;

public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;
	private String nome;
	private Tipo tipo;
	
	public Usuario(String nome, Tipo tipo) {
		super();
		this.nome = nome;
		this.tipo = tipo;
	}

	public enum Tipo {
		SINDICO, CLIENTE
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void setTipo(Tipo tipo) {
		this.tipo = tipo;
	}
	
	
}
