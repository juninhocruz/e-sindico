package comum;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface UpdateListener extends Remote {
	void update(List<Transacao> historico) throws RemoteException;
}
