package server;

import java.net.MalformedURLException;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import comum.C;

public class Main {

	public static void main(String[] args) throws RemoteException, MalformedURLException, AlreadyBoundException {
		Gestor gestor = new Gestor();
		
		String host = C.NAME_SERVER;
		int port = C.PORT_SERVER;
		
		System.setProperty("java.rmi.server.hostname",C.IP_SERVER);
		
		System.out.print("Iniciando servidor na porta " + port + "... ");
		Registry registry = LocateRegistry.createRegistry(port);
		registry.bind(host, gestor);
		System.out.print("OK!");
	}

}
