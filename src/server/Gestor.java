package server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

import comum.Mensageiro;
import comum.Transacao;
import comum.UpdateListener;
import comum.Usuario;

public class Gestor extends UnicastRemoteObject implements Mensageiro {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Transacao> historico;
	private ArrayList<UpdateListener> listeners;
	private double saldo;

	public Gestor() throws RemoteException {
		super();
		historico = new ArrayList<>();
		listeners = new ArrayList<>();
		saldo = 0;
	}

	@Override
	public String hello(String nome) throws RemoteException {
		return "Olá, " + nome + "!";
	}

	@Override
	public List<Transacao> saque(Usuario usuario, double valor) {
		if(usuario.getTipo() != Usuario.Tipo.SINDICO)
			return null;
		
		if(valor > saldo)
			return null;
		
		saldo -= valor;
		Transacao transacao = new Transacao(usuario, valor, Transacao.Tipo.SAQUE, saldo);
		historico.add(transacao);
		notifier();
		
		return historico;
	}

	@Override
	public List<Transacao> deposito(Usuario usuario, double valor) {
		if(usuario.getTipo() != Usuario.Tipo.SINDICO)
			return null;
		
		saldo += valor;
		Transacao transacao = new Transacao(usuario, valor, Transacao.Tipo.DEPOSITO, saldo);
		historico.add(transacao);
		notifier();
		
		return historico;
	}

	private void notifier() {
		for(UpdateListener listener:listeners) {
			if(listener != null) {
				try {
					listener.update(historico);
				}
				catch(RemoteException e) {
					e.printStackTrace();
				}
			}
			else
				listeners.remove(listener);
		}
	}

	@Override
	public List<Transacao> addUpdateListener(UpdateListener listener) {
		if(listener != null)
			listeners.add(listener);
		
		return historico;
	}

}
