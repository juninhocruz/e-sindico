package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import comum.C;
import comum.Mensageiro;
import comum.Usuario;
import comum.Usuario.Tipo;

public class ClientApp extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfEmail;
	private JLabel lblEmail;
	private Registry registry;
	private Mensageiro gerente;
	private JRadioButton radioUser, radioSindico;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientApp frame = new ClientApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ClientApp() {
		
		super("E-Síndico");
		setTitle("e-Síndico");
		setBackground(Color.WHITE);
		String ip = C.IP_SERVER;
		int port = C.PORT_SERVER;
		String host = C.NAME_SERVER;
		
		try {
			registry = LocateRegistry.getRegistry(ip, port);
			
			gerente = (Mensageiro) registry.lookup(host);
		} catch (RemoteException e) {
			JOptionPane.showMessageDialog(null, "001 - Erro ao conectar servidor.", "Algo deu errado! XD", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (NotBoundException e) {
			JOptionPane.showMessageDialog(null, "002 - Erro ao conectar servidor.", "Algo deu errado!", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 200);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);;
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblLogin = new JLabel("Login");
		panel.add(lblLogin);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBackground(Color.WHITE);
		contentPane.add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_1.add(panel_2, BorderLayout.NORTH);
		
		lblEmail = new JLabel("Email:   ");
		lblEmail.setBackground(Color.WHITE);
		panel_2.add(lblEmail);
		
		tfEmail = new JTextField();
		tfEmail.setBackground(Color.WHITE);
		panel_2.add(tfEmail);
		tfEmail.setColumns(20);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_1.add(panel_3, BorderLayout.SOUTH);
		
		JLabel lblDesenvolvidoPorAntnio = new JLabel("Desenvolvido por: António Cruz Jr, Osmar Kabashima Jr e Eduardo Carvalho");
		panel_3.add(lblDesenvolvidoPorAntnio);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBackground(Color.WHITE);
		panel_1.add(panel_4, BorderLayout.CENTER);
		panel_4.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_5 = new JPanel();
		panel_5.setBackground(Color.WHITE);
		panel_4.add(panel_5, BorderLayout.NORTH);
		
		ButtonGroup group = new ButtonGroup();
		radioUser = new JRadioButton("Usuário");
		radioUser.setForeground(SystemColor.activeCaptionText);
		group.add(radioUser);
		panel_5.add(radioUser);
		
		radioSindico = new JRadioButton("Síndico");
		group.add(radioSindico);
		panel_5.add(radioSindico);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBackground(Color.WHITE);
		panel_4.add(panel_6, BorderLayout.CENTER);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		panel_7.setBackground(Color.WHITE);
		panel_6.add(panel_7, BorderLayout.NORTH);
		
		JButton btnEntrar = new JButton("Entrar");
		btnEntrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEntrar.setBackground(Color.CYAN);
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				sendMessage();
			}
		});
		
		panel_7.add(btnEntrar);
	}
	
	private void sendMessage() {
		String name = tfEmail.getText();
		
		if(name.length() == 0 || (!radioUser.isSelected() && !radioSindico.isSelected())) {
			JOptionPane.showMessageDialog(null, "Preencha os campos para entrar.", "Erro!", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		String resposta = "Sem resposta";
		try {
			resposta = gerente.hello(name);
		} catch (RemoteException e) {
			JOptionPane.showMessageDialog(null, "003 - Erro ao obter resposta do servidor.", "Algo deu errado!", JOptionPane.ERROR_MESSAGE);
		}
		
		boolean isSindico = radioSindico.isSelected();
		JOptionPane.showMessageDialog(null, resposta, "Sucesso!", JOptionPane.INFORMATION_MESSAGE);
		startApp(name, isSindico);
	}
	
	private void startApp(String nome, boolean isSindico) {
		Tipo type = isSindico?Tipo.SINDICO:Tipo.CLIENTE;
		Usuario user = new Usuario(nome, type);
		new ESindicoFrame(this, gerente, user);
	}
}
