package client;

import java.awt.BorderLayout;
import java.awt.TextArea;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import comum.Mensageiro;
import comum.Transacao;
import comum.UpdateListener;
import comum.Usuario;
import comum.Usuario.Tipo;

public class ESindicoFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField tfUserName;
	private Mensageiro gerente;
	private Usuario user;
	private JFormattedTextField formattedTfValue;
	private TextArea textAreaHistorico;
	private JFrame home;

	/**
	 * Create the frame.
	 */
	public ESindicoFrame(JFrame home2, Mensageiro gerente, Usuario user) {
		super("e-Síndico");
		this.home = home2;
		this.gerente = gerente;
		this.user = user;
		this.home.setVisible(false);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.NORTH);
		
		tfUserName = new JTextField();
		tfUserName.setText(user.getNome());
		tfUserName.setEditable(false);
		panel.add(tfUserName);
		tfUserName.setColumns(10);
		
		JButton btnSair = new JButton("Sair");
		btnSair.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				home.setVisible(true);
				dispose();
			}
		});
		panel.add(btnSair);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		
		JLabel lblDesenvolvidoPorAntnio = new JLabel("Desenvolvido por: António Cruz Jr, Osmar Kabashima Jr e Eduardo Carvalho");
		panel_1.add(lblDesenvolvidoPorAntnio);
		
		JPanel panel_2 = new JPanel();
		contentPane.add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_3 = new JPanel();
		panel_2.add(panel_3, BorderLayout.SOUTH);
		
		formattedTfValue = new JFormattedTextField();
		formattedTfValue.setColumns(10);
		
		JButton btnSacar = new JButton("Sacar");
		JButton btnDepositar = new JButton("Depositar");
		
		btnSacar.addMouseListener(new SacarAction());
		btnDepositar.addMouseListener(new DepositarAction());
		
		if(user.getTipo() == Tipo.SINDICO) {
			panel_3.add(btnSacar);
			panel_3.add(btnDepositar);
			panel_3.add(formattedTfValue);
		}
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4, BorderLayout.NORTH);
		
		JLabel lblEsndico = new JLabel("e-Síndico 1.0");
		panel_4.add(lblEsndico);
		
		JPanel panel_5 = new JPanel();
		panel_2.add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BorderLayout(0, 0));
		
		textAreaHistorico = new TextArea();
		panel_5.add(textAreaHistorico, BorderLayout.CENTER);
		
		try {
			List<Transacao> listaInicial = gerente.addUpdateListener(new SindicoUpdateListener());
			setHistorico(listaInicial);
		}
		catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Falha na conexão.", "Erro ao logar", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			this.home.setVisible(true);
			dispose();
		}
		
		setVisible(true);
	}

	private class SindicoUpdateListener implements UpdateListener {
		public SindicoUpdateListener() {
			try {
				UnicastRemoteObject.exportObject(this, 8060);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		@Override
		public void update(List<Transacao> historico) throws RemoteException {
			setHistorico(historico);
		}
	}
	
	private class SacarAction extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			double valor = 0;
			try {
				valor = Double.parseDouble(formattedTfValue.getText());
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null, "ENTRADA INVALIDA!", "ERRO!", JOptionPane.ERROR_MESSAGE);
				return;
			}
			System.out.println("Valor = " + valor);
			try {
				List<Transacao> historico = gerente.saque(user, valor);
				if(historico != null)
					setHistorico(historico);
				else
					JOptionPane.showMessageDialog(null, "Não foi possível sacar. Saldo insuficiente.", "Erro ao sacar", JOptionPane.ERROR_MESSAGE);
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Não foi possível sacar. Falha na conexão.", "Erro ao sacar", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	private class DepositarAction extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			double valor = 0;
			try {
				valor = Double.parseDouble(formattedTfValue.getText());
			} catch(Exception e) {
				JOptionPane.showMessageDialog(null, "ENTRADA INVALIDA!", "ERRO!", JOptionPane.ERROR_MESSAGE);
				return;
			}
			System.out.println("Valor = " + valor);
			try {
				List<Transacao> historico = gerente.deposito(user, valor);
				if(historico != null)
					setHistorico(historico);
				else
					JOptionPane.showMessageDialog(null, "Não foi possível efetuar depósito. verifique a conexão.", "Erro ao depositar", JOptionPane.ERROR_MESSAGE);
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Não foi possível depositar. Falha na conexão.", "Erro ao depositar", JOptionPane.ERROR_MESSAGE);
				e.printStackTrace();
			}
		}
	}
	
	private void setHistorico(List<Transacao> historico) {
		String text = "";
		for(Transacao transacao:historico) {
			text += transacao.toString();
		}
		textAreaHistorico.setText(text);
	}
}
